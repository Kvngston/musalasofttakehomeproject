# Drone Delivery Service - REST API

This project is a drone delivery service that allows clients to communicate with the fleet of drones through a REST API. The service provides a way for clients to register drones, load drones with medication items, check loaded medication items for a given drone, check available drones for loading, and check drone battery level for a given drone.

## Technical Details
- Language: Java 17
- Data Format: JSON
- Database: MySQL (with Docker)
- Build Tool: Gradle

## Functionality
    All required endpoints, a few assumptions to make application realistic

- When a drone is loaded, it goes from IDLE to LOADING state
- A job updates Drones that are in LOADING state to LOADED state
- When loading a drone, delivery details are collect, time and address
- A job delivers medications at scheduled delivery time and updates status to DELIVERING state
- A job updates DELIVERING state drones to DELIVERED state
- A job updates DELIVERED state drones to RETURNING 
- A job updates drones back to IDLE from RETURNING
- Audit logs are kept in a table to keep track of drones
- Logs are printed to console to mark milestones and keep track for monitoring
- Table to keep track of history of drones and deliveries done
- Each drone delivery takes about 15% battery (could be refined abit better depending on delivery address, weight of medications)

## Constraints
- Prevent the drone from being loaded with more weight than it can carry
- Prevent the drone from being in LOADING state if the battery level is below 25%
- Introduce a periodic task to check drones battery levels and create history/audit event log

## Getting Started

### Prerequisites
- Java 17
- Gradle 7 or above
- Docker
- Docker Compose
- MySQL 8.0 or above

### Building the Project
1. Clone the project to your local machine.
2. Navigate to the root directory of the project.
3. Run the following command to build the project:


```gradle clean build```


### Running the Project

1. Start the MySQL database using Docker Compose: 

    `cd docker`
   `docker-compose up -d`

2. Open project in favorite IDE intellij preferably
3. Run project 


```note app runs on port 8080, please free up port locally to avoid failures```

    
    
