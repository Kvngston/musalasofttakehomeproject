package com.tk.musalasofttakehomeproject.jobs;

import com.tk.musalasofttakehomeproject.entity.AuditLogs;
import com.tk.musalasofttakehomeproject.entity.Drone;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import com.tk.musalasofttakehomeproject.repository.AuditLogsRepository;
import com.tk.musalasofttakehomeproject.repository.DeliveryDetailsRepository;
import com.tk.musalasofttakehomeproject.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Predicate;

@Component
@Slf4j
@RequiredArgsConstructor
public class DroneJobs {

    private final DroneRepository droneRepository;
    private final DeliveryDetailsRepository deliveryDetailsRepository;
    private final AuditLogsRepository auditLogsRepository;

    private String AUDIT_MESSAGE_CHANGE_STATE = "Updated Drone with id %s to state %s at %s";
    private String AUDIT_MESSAGE_OUT_FOR_DELIVERY = "Drone with id %s and serial number %s out to deliver medications at %s, time %s";

    @Scheduled(cron = "0 * * * * *") // cron runs every minute
    private void updateDroneStatusToLoaded(){
        log.info("Updating Qualified Drones to Loaded state");

        var drones = droneRepository.findByDroneState(State.LOADING);

        drones.forEach(drone -> {
            drone.setDroneState(State.LOADED);
            droneRepository.save(drone);
            var timeStamp = LocalDateTime.now();
            var auditLog = AuditLogs.builder()
                    .auditTime(timeStamp)
                    .uuid(UUID.randomUUID())
                    .audit(String.format(AUDIT_MESSAGE_CHANGE_STATE, drone.getId(), State.LOADED, timeStamp))
                    .build();

            auditLogsRepository.save(auditLog);
        });
    }

    @Scheduled(cron = "0 * * * * *") //runs every 30mins and picks delivery within that interval
    private void updateDroneStatusToDelivering(){
        //Deduct Battery percentage
        log.info("Delivering medications with Drones at scheduled time");


        var now = LocalDateTime.now();
        var plus30Mins = now.plusMinutes(30);


        Predicate<Drone> dronePredicate = drone -> drone.getDeliveryDetails().getDeliveryTime().isAfter(now) && drone.getDeliveryDetails().getDeliveryTime().isBefore(plus30Mins);

        var drones = droneRepository.findByDroneState(State.LOADED).stream().filter(dronePredicate);

        drones.forEach(drone -> {
            drone.setBatteryPercentage(drone.getBatteryPercentage() - 15);
            drone.setDroneState(State.DELIVERING);
            droneRepository.save(drone);

            var timeStamp = LocalDateTime.now();
            var auditLog = AuditLogs.builder()
                    .auditTime(timeStamp)
                    .uuid(UUID.randomUUID())
                    .audit(String.format(AUDIT_MESSAGE_OUT_FOR_DELIVERY, drone.getId(), drone.getSerialNumber(), drone.getDeliveryDetails().getDeliveryAddress(), timeStamp))
                    .build();

            auditLogsRepository.save(auditLog);
        });
    }


    @Scheduled(cron = "0 * * * * *") // runs every minute
    private void updateDroneStatusToDeliveredAtDeliveryTime(){
        log.info("Updating qualified Drones to Delivering State");

        var drones = droneRepository.findByDroneState(State.DELIVERING);

        drones.forEach(drone -> {
            drone.setDroneState(State.DELIVERED);
            droneRepository.save(drone);

            var timeStamp = LocalDateTime.now();
            var auditLog = AuditLogs.builder()
                    .auditTime(timeStamp)
                    .uuid(UUID.randomUUID())
                    .audit(String.format(AUDIT_MESSAGE_CHANGE_STATE, drone.getId(), State.DELIVERED, timeStamp))
                    .build();

            auditLogsRepository.save(auditLog);
        });
    }

    @Scheduled(cron = "0 * * * * *")
    private void updateDroneStatusToReturning(){
        log.info("Updating qualified drones to Returning State");

        var drones = droneRepository.findByDroneState(State.DELIVERED);

        drones.forEach(drone -> {
            drone.setDroneState(State.RETURNING);
            droneRepository.save(drone);

            var timeStamp = LocalDateTime.now();
            var auditLog = AuditLogs.builder()
                    .auditTime(timeStamp)
                    .uuid(UUID.randomUUID())
                    .audit(String.format(AUDIT_MESSAGE_CHANGE_STATE, drone.getId(), State.RETURNING, timeStamp))
                    .build();

            auditLogsRepository.save(auditLog);
        });
    }

    @Scheduled(cron = "0 * * * * *")
    private void postDelivery(){
        //back to IDLE
        log.info("Updating Drones with post Delivery Strategy");
        var drones = droneRepository.findByDroneState(State.RETURNING);

        drones.forEach(drone -> {
            drone.setDroneState(State.IDLE);
            drone.setDeliveryDetails(null);
            drone.setMedications(new ArrayList<>());
            drone.setWeight(0);
            droneRepository.save(drone);

            var timeStamp = LocalDateTime.now();
            var auditLog = AuditLogs.builder()
                    .auditTime(timeStamp)
                    .uuid(UUID.randomUUID())
                    .audit(String.format(AUDIT_MESSAGE_CHANGE_STATE, drone.getId(), State.IDLE, timeStamp))
                    .build();

            auditLogsRepository.save(auditLog);
        });

    }

}
