package com.tk.musalasofttakehomeproject.exception;

import lombok.Getter;

@Getter
public class NotFoundException extends RuntimeException{
    private final String code;
    private final String message;

    public NotFoundException(ErrorCode errorCode, Object... params){
        super(String.format(errorCode.getMessage(), params));
        this.code = errorCode.getCode();
        this.message = String.format(errorCode.getMessage(), params);
    }
}
