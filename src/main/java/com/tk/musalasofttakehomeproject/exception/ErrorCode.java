package com.tk.musalasofttakehomeproject.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

@Getter
@AllArgsConstructor
public enum ErrorCode {

    NOT_FOUND("NOT_FOUND", "%s not found"),
    TOTAL_WEIGHT_ABOVE_MAX_ALLOWED_WEIGHT("TOTAL_WEIGHT_ABOVE_MAX_ALLOWED_WEIGHT", "Max weight exceeded"),
    BATTERY_LOW("BATTERY_LOW", "Drone battery lower than 25%, cannot load drone"),
    DRONE_NOT_READY_FOR_LOADING("DRONE_NOT_READY_FOR_LOADING", "Drone not in loading state"),
    DUPLICATE_ENTITY("DUPLICATE_ENTITY", "Cannot add duplicates"),
    DRONE_HAS_NO_PENDING_MEDICATIONS("DRONE_HAS_NO_PENDING_MEDICATIONS", "Drone has no pending medications to be delivered");

    private final String code;
    private final String message;


    private static Optional<ErrorCode> getByCode(final String code) {
        for (final ErrorCode errorCode : values()) {
            if (errorCode.getCode().equals(code)) {
                return Optional.of(errorCode);
            }
        }
        return Optional.empty();
    }

}
