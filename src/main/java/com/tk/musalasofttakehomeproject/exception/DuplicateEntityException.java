package com.tk.musalasofttakehomeproject.exception;

import lombok.Getter;

@Getter
public class DuplicateEntityException extends RuntimeException {
    private final String code;
    private final String message;

    public DuplicateEntityException(ErrorCode errorCode){
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }
}
