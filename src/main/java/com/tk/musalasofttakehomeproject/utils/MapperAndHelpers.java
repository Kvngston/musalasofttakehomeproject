package com.tk.musalasofttakehomeproject.utils;

import com.tk.musalasofttakehomeproject.entity.Drone;
import com.tk.musalasofttakehomeproject.entity.Medication;
import com.tk.musalasofttakehomeproject.entity.dtos.DroneRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.DroneResponse;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationResponse;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.UUID;

@Service
public class MapperAndHelpers {

    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";

    private static final String DATA = CHAR_LOWER + CHAR_UPPER + NUMBER;
    private static final SecureRandom random = new SecureRandom();

    public static String generateSerialNumber() {
        //might be unnecessary
        StringBuilder sb = new StringBuilder(16);
        for (int i = 0; i < 16; i++) {
            int rndCharAt = random.nextInt(DATA.length());
            char rndChar = DATA.charAt(rndCharAt);
            sb.append(rndChar);
        }
        return sb.toString();
    }

    public Medication mapToMedication(MedicationRequest request){
        return Medication.builder()
                .code(request.code())
                .name(request.name())
                .weight(request.weight())
                .build();
    }

    public MedicationResponse mapToMedicationResponse(Medication medication){
        return MedicationResponse.builder()
                .code(medication.getCode())
                .name(medication.getName())
                .weight(medication.getWeight())
                .id(medication.getId())
                .build();
    }

    public Medication mapToMedication(MedicationResponse medicationResponse){
        return Medication.builder()
                .code(medicationResponse.code())
                .name(medicationResponse.name())
                .weight(medicationResponse.weight())
                .id(medicationResponse.id())
                .build();
    }


    public Drone mapToDrone(DroneRequest droneRequest){
        return Drone.builder()
                .model(droneRequest.model())
                .serialNumber(generateSerialNumberUUID())
                .medications(new ArrayList<>())
                .weight(0)
                .build();
    }

    public DroneResponse mapToDroneResponse(Drone drone){

        var medicationResponse = drone.getMedications().stream().map(this::mapToMedicationResponse).toList();

        return DroneResponse.builder()
                .id(drone.getId())
                .model(drone.getModel())
                .droneState(drone.getDroneState())
                .serialNumber(drone.getSerialNumber())
                .batteryPercentage(drone.getBatteryPercentage())
                .weight(drone.getWeight())
                .deliveryDetails(drone.getDeliveryDetails())
                .medications(medicationResponse)
                .build();
    }


    public String generateSerialNumberUUID(){
        return UUID.randomUUID().toString();
    }
}
