package com.tk.musalasofttakehomeproject.repository;

import com.tk.musalasofttakehomeproject.entity.DroneToDeliveryDestinationHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DroneToDeliveryDestinationHistoryRepository extends JpaRepository<DroneToDeliveryDestinationHistory, Long> {
}
