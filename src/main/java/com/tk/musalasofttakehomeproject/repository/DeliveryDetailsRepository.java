package com.tk.musalasofttakehomeproject.repository;

import com.tk.musalasofttakehomeproject.entity.DeliveryDetails;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface DeliveryDetailsRepository extends JpaRepository<DeliveryDetails, Long> {

    Optional<DeliveryDetails> findById(long id);

    List<DeliveryDetails> findByDeliveryTimeBefore(LocalDateTime time);
}
