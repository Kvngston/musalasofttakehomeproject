package com.tk.musalasofttakehomeproject.repository;

import com.tk.musalasofttakehomeproject.entity.Drone;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DroneRepository extends JpaRepository<Drone, Long> {

    Optional<Drone> findById(long id);

    List<Drone> findByDroneState(State state);


}
