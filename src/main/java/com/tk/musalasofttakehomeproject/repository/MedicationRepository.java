package com.tk.musalasofttakehomeproject.repository;

import com.tk.musalasofttakehomeproject.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MedicationRepository extends JpaRepository<Medication, Long> {

    Optional<Medication> findById(long id);
    Optional<Medication> findByNameAndCode(String name, String code);

}
