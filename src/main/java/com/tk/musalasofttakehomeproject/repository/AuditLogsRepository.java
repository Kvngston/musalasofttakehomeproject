package com.tk.musalasofttakehomeproject.repository;

import com.tk.musalasofttakehomeproject.entity.AuditLogs;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface AuditLogsRepository extends JpaRepository<AuditLogs, Long> {

    Optional<AuditLogs> findById(long id);
    Optional<AuditLogs> findByUuid(UUID uuid);
}
