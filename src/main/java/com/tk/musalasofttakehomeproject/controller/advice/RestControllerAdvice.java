package com.tk.musalasofttakehomeproject.controller.advice;

import com.tk.musalasofttakehomeproject.entity.dtos.Error;
import com.tk.musalasofttakehomeproject.exception.DuplicateEntityException;
import com.tk.musalasofttakehomeproject.exception.NotFoundException;
import com.tk.musalasofttakehomeproject.exception.RequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
@Slf4j
public class RestControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(
            NotFoundException ex, WebRequest request) {

        var error = Error.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .status(false)
                .timeStamp(LocalDateTime.now())
                .build();


        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(DuplicateEntityException.class)
    public ResponseEntity<Object> handleDuplicateEntityException(
            DuplicateEntityException ex, WebRequest request) {

        var error = Error.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .status(false)
                .timeStamp(LocalDateTime.now())
                .build();


        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(RequestException.class)
    public ResponseEntity<Object> handleRequestException(
            RequestException ex, WebRequest request) {

        var error = Error.builder()
                .code(ex.getCode())
                .message(ex.getMessage())
                .status(false)
                .timeStamp(LocalDateTime.now())
                .build();


        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    /**
     * @param ex      the exception to handle
     * @param headers the headers to use for the response
     * @param status  the status code to use for the response
     * @param request the current request
     * @return
     */
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatusCode status, WebRequest request) {
        log.error(ex.getMessage());
        var error = Error.builder()
                .code("INVALID_REQUEST_BODY")
                .message("Invalid request body, please check fields")
                .status(false)
                .timeStamp(LocalDateTime.now())
                .build();

        return handleExceptionInternal(ex, error, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
