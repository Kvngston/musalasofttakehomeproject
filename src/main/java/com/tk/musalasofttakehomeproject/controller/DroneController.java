package com.tk.musalasofttakehomeproject.controller;

import com.tk.musalasofttakehomeproject.entity.dtos.*;
import com.tk.musalasofttakehomeproject.service.DroneService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/drone")
@RequiredArgsConstructor
public class DroneController {

    private final DroneService droneService;

    @PostMapping
    public DroneResponse registerADrone(@RequestBody @Valid DroneRequest request){
        return droneService.registerADrone(request);
    }

    @PutMapping("/{id}")
    public DroneResponse editMedication(@RequestBody @Valid DroneRequest request, @PathVariable long id){
        return droneService.editDroneDetails(id, request);
    }


    @PostMapping("/loadDrone/{id}")
    public DroneResponse loadDrone(@RequestBody @Valid LoadDroneRequest request, @PathVariable long id){
        return droneService.loadADrone(id, request);
    }

    @GetMapping("/getMedicationsForDrone/{id}")
    public List<MedicationResponse> getMedicationsForDrone(@PathVariable long id){
        return droneService.getMedicationsForDrone(id);
    }

    @GetMapping("/getAvailableDrones")
    public List<DroneResponse> getAvailableDrones(){
        return droneService.getAvailableDrones();
    }

    @GetMapping("/getBatteryLevel/{id}")
    public DroneResponse getBatteryLevel( @PathVariable long id){
        return droneService.getBatteryLevelForDrone(id);
    }

}
