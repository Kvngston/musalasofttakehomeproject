package com.tk.musalasofttakehomeproject.controller;

import com.tk.musalasofttakehomeproject.entity.dtos.MedicationRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationResponse;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationUpdateRequest;
import com.tk.musalasofttakehomeproject.service.MedicationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/medication")
@RequiredArgsConstructor
public class MedicationController {

    private final MedicationService medicationService;

    @PostMapping
    public MedicationResponse createMedication(@RequestBody @Valid MedicationRequest request){
        return medicationService.createMedication(request);
    }

    @PutMapping("/{id}")
    public MedicationResponse editMedication(@RequestBody @Valid MedicationUpdateRequest request, @PathVariable long id){
        return medicationService.editMedication(id, request);
    }


    @DeleteMapping("/{id}")
    public boolean deleteMedication(@PathVariable long id){
        medicationService.deleteMedication(id);
        return true;
    }
}
