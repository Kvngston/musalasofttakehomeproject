package com.tk.musalasofttakehomeproject.entity;

import com.tk.musalasofttakehomeproject.entity.enums.Model;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Drone implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String serialNumber;
    @Enumerated(EnumType.STRING)
    private Model model;

    private double weight;

    @Builder.Default
    private int batteryPercentage = 100;

    @Enumerated(EnumType.STRING)
    @Builder.Default
    private State droneState = State.IDLE;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Medication> medications;

    @OneToOne
    private DeliveryDetails deliveryDetails;

}

