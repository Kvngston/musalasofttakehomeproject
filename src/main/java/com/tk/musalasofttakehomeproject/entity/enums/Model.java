package com.tk.musalasofttakehomeproject.entity.enums;

public enum Model {
    LIGHTWEIGHT,
    MIDDLEWEIGHT,
    CRUISERWEIGHT,
    HEAVYWEIGHT
}
