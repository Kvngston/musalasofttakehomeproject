package com.tk.musalasofttakehomeproject.entity.enums;

public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
