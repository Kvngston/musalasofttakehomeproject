package com.tk.musalasofttakehomeproject.entity.dtos;

import lombok.Builder;

@Builder
public record MedicationResponse(Long id, String name, double weight, String code) {
}
