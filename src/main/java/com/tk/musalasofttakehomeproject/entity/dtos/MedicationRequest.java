package com.tk.musalasofttakehomeproject.entity.dtos;

import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Builder;


@Builder
public record MedicationRequest(@NotBlank @Pattern(regexp = "[a-zA-Z0-9_-]+") String name, @PositiveOrZero double weight, @NotBlank @Pattern(regexp = "[A-Z0-9_]+") String code, @Nullable Byte[] image) {
}
