package com.tk.musalasofttakehomeproject.entity.dtos;

import jakarta.validation.constraints.PositiveOrZero;

public record MedicationUpdateRequest(String name, @PositiveOrZero double weight, String code) {
}
