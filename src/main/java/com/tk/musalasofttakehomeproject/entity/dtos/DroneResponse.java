package com.tk.musalasofttakehomeproject.entity.dtos;

import com.tk.musalasofttakehomeproject.entity.DeliveryDetails;
import com.tk.musalasofttakehomeproject.entity.enums.Model;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import lombok.Builder;

import java.util.List;


@Builder
public record DroneResponse(Long id, String serialNumber, Model model, double weight, int batteryPercentage, State droneState, List<MedicationResponse> medications, DeliveryDetails deliveryDetails) {}
