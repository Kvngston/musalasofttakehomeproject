package com.tk.musalasofttakehomeproject.entity.dtos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Builder;

import java.time.LocalDateTime;
import java.util.List;

@Builder
public record LoadDroneRequest(List<MedicationRequest> medicationRequests, String deliveryAddress,
                               @JsonDeserialize(using = LocalDateTimeDeserializer.class)
                               @JsonSerialize(using = LocalDateTimeSerializer.class) LocalDateTime deliveryTime) {
}
