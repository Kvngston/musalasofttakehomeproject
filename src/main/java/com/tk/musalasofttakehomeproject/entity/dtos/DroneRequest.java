package com.tk.musalasofttakehomeproject.entity.dtos;

import com.tk.musalasofttakehomeproject.entity.enums.Model;

public record DroneRequest(Model model) { //TODO try to use the enum validator
}
