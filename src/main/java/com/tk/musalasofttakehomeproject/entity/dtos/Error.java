package com.tk.musalasofttakehomeproject.entity.dtos;

import lombok.Builder;

import java.time.LocalDateTime;

@Builder
public record Error(Boolean status, String code, String message, LocalDateTime timeStamp) {
}
