package com.tk.musalasofttakehomeproject.annotations;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.List;

public class ModelValuesValidatorImpl implements ConstraintValidator<ModelValidator, String> {
    private List<String> valueList = null;

    @Override
    public void initialize(ModelValidator constraintAnnotation) {
        valueList = List.of(Arrays.toString(constraintAnnotation.enumClass().getEnumConstants()));
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || valueList.contains(value);
    }
}
