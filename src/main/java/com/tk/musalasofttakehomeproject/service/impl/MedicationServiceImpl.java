package com.tk.musalasofttakehomeproject.service.impl;

import com.tk.musalasofttakehomeproject.entity.dtos.MedicationRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationResponse;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationUpdateRequest;
import com.tk.musalasofttakehomeproject.exception.DuplicateEntityException;
import com.tk.musalasofttakehomeproject.exception.ErrorCode;
import com.tk.musalasofttakehomeproject.exception.NotFoundException;
import com.tk.musalasofttakehomeproject.repository.MedicationRepository;
import com.tk.musalasofttakehomeproject.service.MedicationService;
import com.tk.musalasofttakehomeproject.utils.MapperAndHelpers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;
    private final MapperAndHelpers mapperAndHelpers;

    /**
     * @param medicationRequest
     * @return
     */
    @Override
    public MedicationResponse createMedication(MedicationRequest medicationRequest) {
        medicationRepository.findByNameAndCode(medicationRequest.name(), medicationRequest.code()).ifPresent(medication -> {
            log.error(ErrorCode.DUPLICATE_ENTITY.getMessage());
            throw new DuplicateEntityException(ErrorCode.DUPLICATE_ENTITY);
        });
        var medication = medicationRepository.save(mapperAndHelpers.mapToMedication(medicationRequest));
        return mapperAndHelpers.mapToMedicationResponse(medication);
    }

    /**
     * @param id
     * @param request
     * @return
     */
    @Override
    public MedicationResponse editMedication(long id, MedicationUpdateRequest request) {
        var medication = medicationRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Medication with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Medication with id " + id);
        });

        if (!StringUtils.isBlank(request.name())) {
            medication.setName(request.name());
        }
        if (!StringUtils.isBlank(request.code())) {
            medication.setCode(request.code());
        }

        medication.setWeight(request.weight());
        medicationRepository.save(medication);

        return mapperAndHelpers.mapToMedicationResponse(medication);
    }

    /**
     * @param id
     */
    @Override
    public void deleteMedication(long id) {
        var medication = medicationRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Medication with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Medication with id " + id);
        });
        medicationRepository.delete(medication);
    }
}
