package com.tk.musalasofttakehomeproject.service.impl;

import com.tk.musalasofttakehomeproject.entity.DeliveryDetails;
import com.tk.musalasofttakehomeproject.entity.DroneToDeliveryDestinationHistory;
import com.tk.musalasofttakehomeproject.entity.dtos.*;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import com.tk.musalasofttakehomeproject.exception.ErrorCode;
import com.tk.musalasofttakehomeproject.exception.NotFoundException;
import com.tk.musalasofttakehomeproject.exception.RequestException;
import com.tk.musalasofttakehomeproject.repository.DeliveryDetailsRepository;
import com.tk.musalasofttakehomeproject.repository.DroneRepository;
import com.tk.musalasofttakehomeproject.repository.DroneToDeliveryDestinationHistoryRepository;
import com.tk.musalasofttakehomeproject.repository.MedicationRepository;
import com.tk.musalasofttakehomeproject.service.DroneService;
import com.tk.musalasofttakehomeproject.utils.MapperAndHelpers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DroneServiceImpl implements DroneService {

    private final DroneRepository droneRepository;
    private final MapperAndHelpers mapperAndHelpers;

    private final MedicationRepository medicationRepository;
    private final DeliveryDetailsRepository deliveryDetailsRepository;
    private final DroneToDeliveryDestinationHistoryRepository droneToDeliveryDestinationHistoryRepository;

    /**
     * @param droneRequest
     * @return DroneResponse
     */
    @Override
    public DroneResponse registerADrone(DroneRequest droneRequest) {
        var drone = droneRepository.save(mapperAndHelpers.mapToDrone(droneRequest));
        return mapperAndHelpers.mapToDroneResponse(drone);
    }

    /**
     * @param id
     * @param droneRequest
     * @return DroneResponse
     */
    @Override
    public DroneResponse editDroneDetails(long id, DroneRequest droneRequest) {
        var drone = droneRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Drone with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Drone with id " + id);
        });

        if (drone.getModel().equals(droneRequest.model())) {
            return mapperAndHelpers.mapToDroneResponse(drone);
        }
        drone.setModel(droneRequest.model());
        droneRepository.save(drone);
        return mapperAndHelpers.mapToDroneResponse(drone);
    }

    /**
     * @param loadDroneRequest LoadDroneRequest
     * @return DroneResponse
     */
    @Override
    public DroneResponse loadADrone(long id, LoadDroneRequest loadDroneRequest) {

        //TODO create levels with weight, sort of weight levels, a certain Drone model should have weight levels it can carry

        var drone = droneRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Drone with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Drone with id " + id);
        });

        if (!drone.getDroneState().equals(State.IDLE)) {
            throw new RequestException(ErrorCode.DRONE_NOT_READY_FOR_LOADING);
        }

        var totalWeight = loadDroneRequest.medicationRequests().stream().mapToDouble(MedicationRequest::weight).sum();

        if (totalWeight > 500) {
            throw new RequestException(ErrorCode.TOTAL_WEIGHT_ABOVE_MAX_ALLOWED_WEIGHT);
        }

        if (drone.getBatteryPercentage() < 25) {
            throw new RequestException(ErrorCode.BATTERY_LOW);
        }

        drone.setWeight(totalWeight);
        drone.setDroneState(State.LOADING);
        loadDroneRequest.medicationRequests().forEach(request -> {
            var medication = mapperAndHelpers.mapToMedication(request);
            medicationRepository.save(medication);
            drone.getMedications().add(medication);
        });
        var deliveryDetails = DeliveryDetails.builder()
                .deliveryAddress(loadDroneRequest.deliveryAddress())
                .deliveryTime(loadDroneRequest.deliveryTime()).build();
        deliveryDetailsRepository.save(deliveryDetails);
        drone.setDeliveryDetails(deliveryDetails);

        var droneHistory = DroneToDeliveryDestinationHistory.builder()
                .droneId(drone.getId())
                .deliveryDetailsId(deliveryDetails.getId()).build();

        droneToDeliveryDestinationHistoryRepository.save(droneHistory);

        return mapperAndHelpers.mapToDroneResponse(droneRepository.save(drone));
    }

    /**
     * @param id Drone id
     * @return List of MedicationResponse
     */
    @Override
    public List<MedicationResponse> getMedicationsForDrone(long id) {
        var drone = droneRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Drone with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Drone with id " + id);
        });

        if (drone.getDroneState().equals(State.IDLE)) {
            throw new RequestException(ErrorCode.DRONE_HAS_NO_PENDING_MEDICATIONS);
        }

        return drone.getMedications().stream().map(mapperAndHelpers::mapToMedicationResponse).toList();
    }

    /**
     * @return DroneResponse
     */
    @Override
    public List<DroneResponse> getAvailableDrones() {
        return droneRepository.findByDroneState(State.IDLE).stream().map(mapperAndHelpers::mapToDroneResponse).toList();
    }

    /**
     * @param id
     * @return DroneResponse
     */
    @Override
    public DroneResponse getBatteryLevelForDrone(long id) {
        var drone = droneRepository.findById(id).orElseThrow(() -> {
            log.error(String.format(ErrorCode.NOT_FOUND.getMessage(), "Drone with id " + id));
            throw new NotFoundException(ErrorCode.NOT_FOUND, "Drone with id " + id);
        });
        return mapperAndHelpers.mapToDroneResponse(drone);
    }
}
