package com.tk.musalasofttakehomeproject.service;

import com.tk.musalasofttakehomeproject.entity.dtos.*;

import java.util.List;

public interface DroneService {

    DroneResponse registerADrone(DroneRequest droneRequest);
    DroneResponse editDroneDetails(long id, DroneRequest droneRequest);
    DroneResponse loadADrone(long id, LoadDroneRequest loadDroneRequest);
    List<MedicationResponse> getMedicationsForDrone(long droneId);
    List<DroneResponse> getAvailableDrones();
    DroneResponse getBatteryLevelForDrone(long id);



}
