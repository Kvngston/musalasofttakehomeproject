package com.tk.musalasofttakehomeproject.service;

import com.tk.musalasofttakehomeproject.entity.dtos.MedicationRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationResponse;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationUpdateRequest;

public interface MedicationService {
    MedicationResponse createMedication(MedicationRequest medicationRequest);
    MedicationResponse editMedication(long id, MedicationUpdateRequest request);
    void deleteMedication(long id);
}
