package com.tk.musalasofttakehomeproject.integrationTests.configuration;

import com.tk.musalasofttakehomeproject.MusalaSoftTakeHomeProjectApplication;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.web.client.RestOperations;

import java.net.ServerSocket;

@TestConfiguration
@Import({
        MusalaSoftTakeHomeProjectApplication.class,
        BaseIntegrationTestContainerConfiguration.class
})
public class BaseIntegrationTestConfiguration {

    @Bean
    RestOperations restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    static class WebServerInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        @SneakyThrows
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            try (ServerSocket socket = new ServerSocket(0)) {
                int port = socket.getLocalPort();


                TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                        configurableApplicationContext,
                        "server.port=" + port
                );
            }catch (Exception ignored){

            }
        }
    }

}
