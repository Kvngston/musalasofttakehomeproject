package com.tk.musalasofttakehomeproject.integrationTests.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;

@Configuration
public class BaseIntegrationTestContainerConfiguration {

    private static final int MYSQL_PORT = 3306;

    private static final MySQLContainer<?> mysql = new MySQLContainer<>(pickMysqlImage())
            .withDatabaseName("dronesDb")
            .withExposedPorts(MYSQL_PORT)
            .withUsername("root")
            .withPassword("123456");

    private static boolean runningOnMacOs() {
        return System.getProperty("os.name", "unknown").toLowerCase().contains("mac");
    }


    private static DockerImageName pickMysqlImage() {
        if (runningOnMacOs()) {
            return DockerImageName.parse("mysql:8.0-oracle")
                    .asCompatibleSubstituteFor("mysql");
        } else {
            return DockerImageName.parse("mysql:8.0.20");
        }
    }


    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(@NotNull ConfigurableApplicationContext applicationContext) {
            startIfNotRunning();
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                    applicationContext,
                    "spring.datasource.url=" + mysql.getJdbcUrl(),
                    "spring.datasource.username=" + mysql.getUsername(),
                    "spring.datasource.password=" + mysql.getPassword()
            );
        }


        private static void startIfNotRunning() {
            if (!BaseIntegrationTestContainerConfiguration.mysql.isRunning()) {
                BaseIntegrationTestContainerConfiguration.mysql.start();
            }
        }
    }


}
