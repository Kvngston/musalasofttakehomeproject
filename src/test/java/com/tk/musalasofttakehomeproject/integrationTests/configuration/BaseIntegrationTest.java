package com.tk.musalasofttakehomeproject.integrationTests.configuration;

import com.tk.musalasofttakehomeproject.MusalaSoftTakeHomeProjectApplication;
import com.tk.musalasofttakehomeproject.repository.DroneRepository;
import com.tk.musalasofttakehomeproject.repository.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestOperations;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ContextConfiguration(
        classes = {BaseIntegrationTestConfiguration.class, MusalaSoftTakeHomeProjectApplication.class},
        initializers = {
                BaseIntegrationTestConfiguration.WebServerInitializer.class,
                BaseIntegrationTestContainerConfiguration.Initializer.class
        }
)
public class BaseIntegrationTest {

    @Autowired
    protected MusalaSoftTakeHomeProjectApplication homeProjectApplication;

    @Autowired
    protected DroneRepository droneRepository;

    @Autowired
    protected MedicationRepository medicationRepository;

    @Autowired
    protected RestOperations restOperations;

    @LocalServerPort
    protected int webPort;
}
