package com.tk.musalasofttakehomeproject.integrationTests;

import com.tk.musalasofttakehomeproject.entity.Drone;
import com.tk.musalasofttakehomeproject.entity.Medication;
import com.tk.musalasofttakehomeproject.entity.dtos.*;
import com.tk.musalasofttakehomeproject.entity.enums.Model;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import com.tk.musalasofttakehomeproject.integrationTests.configuration.BaseIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.RequestEntity;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class DroneControllerTest extends BaseIntegrationTest {

    private final String baseUrl = "http://localhost:";


    @Test
    void registerDrone() {
        var droneRequest = new DroneRequest(Model.HEAVYWEIGHT);
        var result = restOperations.exchange(
                        RequestEntity
                                .post(URI.create(baseUrl + webPort + "/api/v1/drone"))
                                .body(droneRequest),
                        new ParameterizedTypeReference<DroneResponse>() {
                        })
                .getBody();

        assertThat(result).isNotNull();
    }

    @Test
    void loadDrone() {
        var drone = Drone.builder()
                .serialNumber(UUID.randomUUID().toString())
                .droneState(State.IDLE)
                .weight(0)
                .deliveryDetails(null)
                .batteryPercentage(100)
                .model(Model.HEAVYWEIGHT)
                .medications(new ArrayList<>())
                .build();

        droneRepository.save(drone);

        var medicationRequest = new MedicationRequest("Test Name", 400, "Test code", null);
        var loadDroneRequest = new LoadDroneRequest(List.of(medicationRequest), "Test address", LocalDateTime.now());
        var result = restOperations.exchange(
                        RequestEntity
                                .post(URI.create(baseUrl + webPort + "/api/v1/drone/loadDrone/"+drone.getId()))
                                .body(loadDroneRequest),
                        new ParameterizedTypeReference<DroneResponse>() {
                        })
                .getBody();

        assertThat(result).isNotNull();
    }

    @Test
    void getMedicationsForDrone() {
        var drone = Drone.builder()
                .serialNumber(UUID.randomUUID().toString())
                .droneState(State.LOADED)
                .weight(0)
                .deliveryDetails(null)
                .batteryPercentage(100)
                .model(Model.HEAVYWEIGHT)
                .medications(new ArrayList<>())
                .build();
        droneRepository.save(drone);

        var medication = Medication.builder()
                .code("Test Code")
                .weight(400)
                .name("Test Med")
                .build();

        medicationRepository.save(medication);
        drone.getMedications().add(medication);

        droneRepository.save(drone);

        var result = restOperations.exchange(
                        RequestEntity
                                .get(URI.create(baseUrl + webPort + "/api/v1/drone/getMedicationsForDrone/"+drone.getId()))
                                .build(),
                        new ParameterizedTypeReference<List<MedicationResponse>>() {
                        })
                .getBody();

        assertThat(result).isNotNull();
    }

    @Test
    void getBatteryLevelForDrone() {
        registerDrone();
        var result = restOperations.exchange(
                        RequestEntity
                                .get(URI.create(baseUrl + webPort + "/api/v1/drone/getBatteryLevel/1"))
                                .build(),
                        new ParameterizedTypeReference<DroneResponse>() {
                        })
                .getBody();

        assertThat(result).isNotNull();
    }

    @Test
    void getAvailableDrones() {
        registerDrone();
        registerDrone();
        var result = restOperations.exchange(
                        RequestEntity
                                .get(URI.create(baseUrl + webPort + "/api/v1/drone/getAvailableDrones"))
                                .build(),
                        new ParameterizedTypeReference<List<DroneResponse>>() {
                        })
                .getBody();

        assertThat(result).isNotNull();
    }
}
