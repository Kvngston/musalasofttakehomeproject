package com.tk.musalasofttakehomeproject.integrationTests;

import com.tk.musalasofttakehomeproject.integrationTests.configuration.BaseIntegrationTest;
import org.junit.jupiter.api.Test;

public class ContextLoadsTest extends BaseIntegrationTest {

    @Test
    public void contextLoads(){
        assert homeProjectApplication != null;
    }
}
