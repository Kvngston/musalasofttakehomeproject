package com.tk.musalasofttakehomeproject.service.impl;

import com.tk.musalasofttakehomeproject.entity.DeliveryDetails;
import com.tk.musalasofttakehomeproject.entity.Drone;
import com.tk.musalasofttakehomeproject.entity.DroneToDeliveryDestinationHistory;
import com.tk.musalasofttakehomeproject.entity.Medication;
import com.tk.musalasofttakehomeproject.entity.dtos.*;
import com.tk.musalasofttakehomeproject.entity.enums.Model;
import com.tk.musalasofttakehomeproject.entity.enums.State;
import com.tk.musalasofttakehomeproject.exception.NotFoundException;
import com.tk.musalasofttakehomeproject.exception.RequestException;
import com.tk.musalasofttakehomeproject.repository.DeliveryDetailsRepository;
import com.tk.musalasofttakehomeproject.repository.DroneRepository;
import com.tk.musalasofttakehomeproject.repository.DroneToDeliveryDestinationHistoryRepository;
import com.tk.musalasofttakehomeproject.repository.MedicationRepository;
import com.tk.musalasofttakehomeproject.utils.MapperAndHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DroneServiceImplTest {

    @InjectMocks
    private DroneServiceImpl droneService;

    @Mock
    private DroneRepository droneRepository;
    @Spy
    private MapperAndHelpers mapperAndHelpers;
    @Mock
    private MedicationRepository medicationRepository;
    @Mock
    private DeliveryDetailsRepository deliveryDetailsRepository;

    @Mock
    private DroneToDeliveryDestinationHistoryRepository droneToDeliveryDestinationHistoryRepository;

    private MedicationRequest medicationRequest;
    private MedicationResponse medicationResponse;
    private MedicationUpdateRequest medicationUpdateRequest;
    private Medication medication;

    private DroneRequest droneRequest;
    private DroneRequest droneUpdateRequest;
    private DroneResponse droneResponse;
    private DroneResponse updateDroneResponse;
    private LoadDroneRequest loadDroneRequest;
    private Drone drone;
    private Drone updatedDrone;

    private DeliveryDetails deliveryDetails;

    @BeforeEach
    void setUp(){
        medicationRequest = new MedicationRequest("Test Name", Double.MAX_VALUE, "Test code", null);
        medicationUpdateRequest = new MedicationUpdateRequest("Change of name", Double.MIN_VALUE, "New Test Code");
        medication = mapperAndHelpers.mapToMedication(medicationRequest);
        medicationResponse = mapperAndHelpers.mapToMedicationResponse(medication);
        droneRequest = new DroneRequest(Model.HEAVYWEIGHT);
        droneUpdateRequest = new DroneRequest(Model.CRUISERWEIGHT);
        drone = mapperAndHelpers.mapToDrone(droneRequest);
        updatedDrone = mapperAndHelpers.mapToDrone(droneUpdateRequest);
        loadDroneRequest = new LoadDroneRequest(List.of(medicationRequest), "Test address", LocalDateTime.MAX);
        droneResponse = mapperAndHelpers.mapToDroneResponse(drone);
        updateDroneResponse = mapperAndHelpers.mapToDroneResponse(updatedDrone);
        deliveryDetails = new DeliveryDetails(Long.MAX_VALUE, loadDroneRequest.deliveryAddress(), loadDroneRequest.deliveryTime());
    }

    @Test
    void registerADrone() {
        when(droneRepository.save(any(Drone.class))).thenReturn(drone);

        var result = droneService.registerADrone(droneRequest);
        assertThat(result).isEqualTo(droneResponse);
    }

    @Test
    void editDroneDetails() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));
        when(droneRepository.save(any(Drone.class))).thenReturn(updatedDrone);

        var result = droneService.editDroneDetails(Long.MAX_VALUE, droneUpdateRequest);
        assertThat(result.model()).isEqualTo(droneUpdateRequest.model());
    }

    @Test
    void editDroneDetails_DroneNotFound() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> droneService.editDroneDetails(Long.MAX_VALUE, droneUpdateRequest));
    }

    @Test
    void loadADrone() {
        medicationRequest = MedicationRequest.builder()
                .code("Test Code")
                .weight(400)
                .name("Test Name")
                .build();
        loadDroneRequest = new LoadDroneRequest(List.of(medicationRequest), "Test address", LocalDateTime.MAX);
        drone.setDroneState(State.IDLE);
        drone.setBatteryPercentage(100);
        drone.setDeliveryDetails(deliveryDetails);
        medication = mapperAndHelpers.mapToMedication(medicationRequest);
        medicationResponse = mapperAndHelpers.mapToMedicationResponse(medication);


        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));
        when(droneRepository.save(any(Drone.class))).thenReturn(drone);
        when(medicationRepository.save(any(Medication.class))).thenReturn(medication);
        when(deliveryDetailsRepository.save(any(DeliveryDetails.class))).thenReturn(deliveryDetails);
        when(droneToDeliveryDestinationHistoryRepository.save(any(DroneToDeliveryDestinationHistory.class))).thenReturn(new DroneToDeliveryDestinationHistory());


        var result = droneService.loadADrone(Long.MAX_VALUE, loadDroneRequest);
        assertThat(result).isNotNull();
        assertThat(result.medications()).asList().contains(medicationResponse);

    }

    @Test
    void loadADrone_NotFound() {

        when(droneRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> droneService.loadADrone(Long.MAX_VALUE, loadDroneRequest));
    }

    @Test
    void loadADrone_MaxWeightExceeded() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));

        assertThrows(RequestException.class, () -> droneService.loadADrone(Long.MAX_VALUE, loadDroneRequest));

    }
    @Test
    void getMedicationsForDrone() {
        drone.setDroneState(State.LOADING);
        drone.setMedications(List.of(medication));
        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));

        var result = droneService.getMedicationsForDrone(Long.MAX_VALUE);

        assertThat(result).isNotNull();
        assertThat(result).asList().contains(medicationResponse);
    }

    @Test
    void getMedicationsForDrone_NotFound() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> droneService.getMedicationsForDrone(Long.MAX_VALUE));
    }

    @Test
    void getMedicationsForDrone_NoPendingMedicationsForDelivery() {
        drone.setDroneState(State.IDLE);
        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));

        assertThrows(RequestException.class, () -> droneService.getMedicationsForDrone(Long.MAX_VALUE));
    }

    @Test
    void getAvailableDrones() {
        drone.setDroneState(State.IDLE);
        updatedDrone.setDroneState(State.IDLE);
        droneResponse = mapperAndHelpers.mapToDroneResponse(drone);
        updateDroneResponse = mapperAndHelpers.mapToDroneResponse(updatedDrone);

        when(droneRepository.findByDroneState(State.IDLE)).thenReturn(List.of(drone, updatedDrone));

        var result = droneService.getAvailableDrones();
        assertThat(result).asList().contains(droneResponse, updateDroneResponse);
    }

    @Test
    void getAvailableDrones_ReturnsEmptyList() {
        when(droneRepository.findByDroneState(State.IDLE)).thenReturn(List.of());

        var result = droneService.getAvailableDrones();
        assertThat(result).asList().isEmpty();
    }

    @Test
    void getBatteryLevelForDrone() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.of(drone));

        var result = droneService.getBatteryLevelForDrone(Long.MAX_VALUE);
        assertThat(result.batteryPercentage()).isEqualTo(drone.getBatteryPercentage());
    }

    @Test
    void getBatteryLevelForDrone_NotFound() {
        when(droneRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> droneService.getBatteryLevelForDrone(Long.MAX_VALUE));
    }
}