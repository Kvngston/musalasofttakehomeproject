package com.tk.musalasofttakehomeproject.service.impl;

import com.tk.musalasofttakehomeproject.entity.Medication;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationRequest;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationResponse;
import com.tk.musalasofttakehomeproject.entity.dtos.MedicationUpdateRequest;
import com.tk.musalasofttakehomeproject.exception.DuplicateEntityException;
import com.tk.musalasofttakehomeproject.exception.NotFoundException;
import com.tk.musalasofttakehomeproject.repository.MedicationRepository;
import com.tk.musalasofttakehomeproject.utils.MapperAndHelpers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicationServiceImplTest {

    @InjectMocks
    private MedicationServiceImpl medicationService;

    @Mock
    private MedicationRepository medicationRepository;

    @Spy
    private MapperAndHelpers mapperAndHelpers;

    private MedicationRequest medicationRequest;
    private MedicationResponse medicationResponse;
    private MedicationUpdateRequest medicationUpdateRequest;
    private Medication medication;
    @BeforeEach
    void setUp(){
        medicationRequest = new MedicationRequest("Test Name", Double.MAX_VALUE, "Test code", null);
        medicationUpdateRequest = new MedicationUpdateRequest("Change of name", Double.MIN_VALUE, "New Test Code");
        medication = mapperAndHelpers.mapToMedication(medicationRequest);
        medicationResponse = mapperAndHelpers.mapToMedicationResponse(medication);
    }

    @Test
    void createMedication() {
        when(medicationRepository.findByNameAndCode(anyString(), anyString())).thenReturn(Optional.empty());
        when(medicationRepository.save(any(Medication.class))).thenReturn(medication);

        var result = medicationService.createMedication(medicationRequest);
        assertThat(result).isEqualTo(medicationResponse);
    }

    @Test
    void createMedication_DuplicateEntity() {
        when(medicationRepository.findByNameAndCode(anyString(), anyString())).thenReturn(Optional.of(medication));

        assertThrows(DuplicateEntityException.class, () -> medicationService.createMedication(medicationRequest));
    }

    @Test
    void editMedication() {
        when(medicationRepository.findById(anyLong())).thenReturn(Optional.of(medication));
        when(medicationRepository.save(any(Medication.class))).thenReturn(medication);

        var result = medicationService.editMedication(Long.MAX_VALUE, medicationUpdateRequest);
        assertThat(result.code()).isEqualTo(medicationUpdateRequest.code());
        assertThat(result.name()).isEqualTo(medicationUpdateRequest.name());
        assertThat(result.weight()).isEqualTo(medicationUpdateRequest.weight());
    }

    @Test
    void editMedication_MedicationNotFound() {
        when(medicationRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> medicationService.editMedication(Long.MAX_VALUE, medicationUpdateRequest));
    }

    @Test
    void deleteMedication() {
        when(medicationRepository.findById(anyLong())).thenReturn(Optional.of(medication));
        doNothing().when(medicationRepository).delete(any(Medication.class));

        medicationService.deleteMedication(Long.MAX_VALUE);
    }

    @Test
    void deleteMedication_MedicationNotFound() {
        when(medicationRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> medicationService.deleteMedication(Long.MAX_VALUE));
    }
}